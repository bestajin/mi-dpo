package cz.cvut.fit.spravaprojektu.usermanager;


import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.NoPermissionException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.NotInRoleException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class UserTest {

    private IRoleManager roleManager;

    private IPermissionChecker permissionChecker;

    private User user;


    @Before
    public void init() {
        roleManager = mock(IRoleManager.class);
        permissionChecker = mock(IPermissionChecker.class);
        user = new User("john", roleManager, permissionChecker);
    }


    @Test
    public void shouldHaveCorrectUsername() {
        assertEquals("john", user.getUserName());
    }


    @Test
    public void shouldBeAuthenticatedIfSetSo() {
        user.setAuthenticated(true);
        assertTrue(user.isAuthenticated());
    }


    @Test
    public void shouldUseRolesManager() {
        when(roleManager.hasRole(user, "administrator")).thenReturn(true);
        assertTrue(user.hasRole("administrator"));
        verify(roleManager, times(1)).hasRole(user, "administrator");
    }


    @Test(expected = NotInRoleException.class)
    public void shouldThrowExceptionIfDoesNotHaveRoleAndCheckRoleIsCalled() throws NotInRoleException {
        when(roleManager.hasRole(any(User.class), anyString())).thenReturn(false);
        user.checkRole("support");
    }


    @Test
    public void shouldUsePermissionChecker() {
        when(permissionChecker.isAllowed(user, "view.projects")).thenReturn(true);
        user.isAllowed("view.projects");
        verify(permissionChecker, times(1)).isAllowed(user, "view.projects");
    }


    @Test(expected = NoPermissionException.class)
    public void shouldThrowExceptionIfCheckAllowedFails() throws AuthenticationException {
        when(permissionChecker.isAllowed(user, "permission")).thenReturn(false);
        user.checkAllowed("permission");
    }
}
