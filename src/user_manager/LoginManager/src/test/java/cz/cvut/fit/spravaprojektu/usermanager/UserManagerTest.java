package cz.cvut.fit.spravaprojektu.usermanager;


import cz.cvut.fit.spravaprojektu.usermanager.exception.AlreadyLoggedInException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.InvalidCredentialsException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.NotAuthenticatedException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.UserNotFoundException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class UserManagerTest {

    private IUser user;

    private IUserRepository userRepository;

    private UserManager userManager;

    private IAuthenticator authenticator;


    @Before
    public void init() {
        user = mock(IUser.class);
        userRepository = mock(IUserRepository.class);
        authenticator = mock(IAuthenticator.class);
        when(userRepository.findUser(anyString())).thenReturn(user);
        when(authenticator.authenticate(any(IUser.class), any(AuthToken.class))).thenReturn(true);
        userManager = new UserManager(userRepository, authenticator);
    }


    @Test
    public void shouldNotHaveCurrentUserBeforeLoginAttempt() {
        assertNull(userManager.getCurrentUser());
    }


    @Test
    public void shouldUserAuthenticatorForAuthenticationChecks() throws AuthenticationException {
        AuthToken token = mock(AuthToken.class);
        userManager.login("frank", token);
        verify(authenticator, times(1)).authenticate(user, token);
    }


    @Test(expected = InvalidCredentialsException.class)
    public void shouldNotAuthenticateUserIfAuthenticatorFails() throws AuthenticationException {
        when(authenticator.authenticate(any(IUser.class), any(AuthToken.class))).thenReturn(false);
        AuthToken token = mock(AuthToken.class);
        userManager.login("carl", token);
    }


    @Test
    public void shouldHaveCurrentUserAfterSuccessfulLogin() throws AuthenticationException {
        AuthToken token = mock(AuthToken.class);
        userManager.login("john", token);
        assertEquals(user, userManager.getCurrentUser());
    }


    @Test(expected = NotAuthenticatedException.class)
    public void shouldThrowExceptionOnLogoutIfNotLoggedIn() throws AuthenticationException {
        when(user.isAuthenticated()).thenReturn(false);
        userManager.logout(user);
    }


    @Test
    public void shouldLogoutIfAuthenticated() throws AuthenticationException {
        when(user.isAuthenticated()).thenReturn(true);
        userManager.logout(user);
        verify(user, times(1)).setAuthenticated(false);
    }


    @Test
    public void shouldNotBeAuthenticatedByDefault() {
        when(user.isAuthenticated()).thenReturn(false);
        assertEquals(user.isAuthenticated(), false);
    }


    @Test(expected = AlreadyLoggedInException.class)
    public void shouldThrowExceptionIfIsAlreadyAuthenticated() throws AuthenticationException {
        AuthToken token = mock(AuthToken.class);
        when(user.isAuthenticated()).thenReturn(true);
        userManager.login("john", token);
    }


    @Test(expected = UserNotFoundException.class)
    public void shouldThrowExceptionIfUserIsNotFound() throws AuthenticationException {
        AuthToken token = mock(AuthToken.class);
        when(userRepository.findUser("john")).thenReturn(null);
        userManager.login("john", token);
    }


    @Test
    public void shouldBeAuthenticatedWhenLoggedIn() throws AuthenticationException {
        AuthToken token = mock(AuthToken.class);
        when(authenticator.authenticate(any(IUser.class), any(AuthToken.class))).thenReturn(true);
        userManager.login("john", token);
        verify(user, times(1)).setAuthenticated(true);
    }


    @Test
    public void shouldChangeCurrentUserOnAnotherSuccessfulLogin() throws AuthenticationException {
        IUser anotherUser = mock(IUser.class);
        AuthToken token = mock(AuthToken.class);
        when(authenticator.authenticate(any(IUser.class), any(AuthToken.class))).thenReturn(true);
        when(userRepository.findUser("john")).thenReturn(user);
        when(userRepository.findUser("alice")).thenReturn(anotherUser);
        userManager.login("john", token);
        assertEquals(user, userManager.getCurrentUser());
        userManager.login("alice", token);
        assertEquals(anotherUser, userManager.getCurrentUser());
    }
}
