package cz.cvut.fit.spravaprojektu.usermanager.factory.stupid;

import cz.cvut.fit.spravaprojektu.usermanager.IPermissionChecker;
import cz.cvut.fit.spravaprojektu.usermanager.IRoleManager;
import cz.cvut.fit.spravaprojektu.usermanager.IUser;
import cz.cvut.fit.spravaprojektu.usermanager.IUserRepository;
import cz.cvut.fit.spravaprojektu.usermanager.User;

/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class StupidUserRepository implements IUserRepository {

    public IUser findUser(String username) {
        IUser user = new User(username, new IRoleManager() {
            public boolean hasRole(IUser user, String roleIdentifier) {
                return true;
            }
        }, new IPermissionChecker() {
            public boolean isAllowed(IUser user, String permission) {
                return true;
            }
        });
        
        return user;
    }

}
