package cz.cvut.fit.spravaprojektu.usermanager;


import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.NoPermissionException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.NotInRoleException;


/**
 * Default implementace IUser modelu
 *
 * @author bestajin@fit.cvut.cz
 */
public class User implements IUser {

    private final String username;

    private final IRoleManager roleManager;

    private IPermissionChecker permissionChecker;

    private boolean authenticated;


    public User(String username, IRoleManager roleManager, IPermissionChecker permissionChecker) {
        this.username = username;
        this.roleManager = roleManager;
        this.permissionChecker = permissionChecker;
    }


    public String getUserName() {
        return username;
    }


    public void setAuthenticated(boolean value) {
        this.authenticated = value;
    }


    public boolean isAuthenticated() {
        return authenticated;
    }


    public boolean isAllowed(String permission) {
        return permissionChecker.isAllowed(this, permission);
    }


    public void checkAllowed(String permission) throws NoPermissionException {
        if (!permissionChecker.isAllowed(this, permission)) {
            throw new NoPermissionException(username + " does not have '" + permission + "' permission");
        }
    }


    public boolean hasRole(String roleIdentifier) {
        return roleManager.hasRole(this, roleIdentifier);
    }


    /**
     * Zkontroluje, jestli má uživatel roli, pokud ne, vyhodí vyjímku.
     *
     * @param roleIdentifier
     * @throws AuthenticationException
     */
    public void checkRole(String roleIdentifier) throws NotInRoleException {
        if (!roleManager.hasRole(this, roleIdentifier)) {
            throw new NotInRoleException(username + " is not in role " + roleIdentifier);
        }
    }

    public UserModel getUserModel() {
        return new UserModel(username);
    }
}
