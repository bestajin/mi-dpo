package cz.cvut.fit.spravaprojektu.usermanager;


import java.util.List;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public interface IRolesRepository {

    /**
     * Vrátí seznam rolí, které jsou přiděleny uživateli s přihlašovacím jménem
     * username.
     *
     * @param username
     * @return
     */
    public List<String> findRoles(String username);
}
