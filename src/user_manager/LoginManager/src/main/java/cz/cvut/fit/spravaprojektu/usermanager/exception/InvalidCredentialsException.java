package cz.cvut.fit.spravaprojektu.usermanager.exception;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class InvalidCredentialsException extends AuthenticationException {

    public InvalidCredentialsException() {
    }


    public InvalidCredentialsException(String string) {
        super(string);
    }


    public InvalidCredentialsException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }


    public InvalidCredentialsException(Throwable thrwbl) {
        super(thrwbl);
    }


    public InvalidCredentialsException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
}
