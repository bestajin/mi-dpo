package cz.cvut.fit.spravaprojektu.usermanager;


import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;


/**
 * @author bestajin@fit.cvut.cz
 */
public interface IUser {

    public UserModel getUserModel();
    
    public String getUserName();


    public void setAuthenticated(boolean value);


    public boolean isAuthenticated();


    public boolean isAllowed(String permission);


    public void checkAllowed(String permission) throws AuthenticationException;


    public boolean hasRole(String roleIdentifier);


    public void checkRole(String roleIdentifier) throws AuthenticationException;
}
