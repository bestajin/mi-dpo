package cz.cvut.fit.spravaprojektu.usermanager.exception;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class AlreadyLoggedInException extends AuthenticationException {

    public AlreadyLoggedInException() {
    }


    public AlreadyLoggedInException(String string) {
        super(string);
    }


    public AlreadyLoggedInException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }


    public AlreadyLoggedInException(Throwable thrwbl) {
        super(thrwbl);
    }


    public AlreadyLoggedInException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
}
