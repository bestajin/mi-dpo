package cz.cvut.fit.spravaprojektu.usermanager.exception;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class NotAuthenticatedException extends AuthenticationException {

    public NotAuthenticatedException() {
    }


    public NotAuthenticatedException(String string) {
        super(string);
    }


    public NotAuthenticatedException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }


    public NotAuthenticatedException(Throwable thrwbl) {
        super(thrwbl);
    }


    public NotAuthenticatedException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
}
