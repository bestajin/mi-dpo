package cz.cvut.fit.spravaprojektu.usermanager;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class PasswordAuthenticator implements IAuthenticator {

    private final IPasswordRepository passwordRepository;


    public PasswordAuthenticator(IPasswordRepository passwordRepository) {
        this.passwordRepository = passwordRepository;
    }


    public boolean authenticate(IUser user, AuthToken authToken) {

        String password = passwordRepository.find(user.getUserName());

        if (password == null) {
            return false;
        }

        Object credentials = authToken.getCredentials();

        if (password.equals(credentials)) {
            return true;
        }

        return false;
    }
}
