package cz.cvut.fit.spravaprojektu.usermanager;


import cz.cvut.fit.spravaprojektu.usermanager.exception.AlreadyLoggedInException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.InvalidCredentialsException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.NotAuthenticatedException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.UserNotFoundException;


/**
 *
 *
 * @author bestajin@fit.cvut.cz
 */
public class UserManager {

    private IUserRepository userRepository;

    private IAuthenticator authenticator;

    private IUser currentUser;


    public UserManager(IUserRepository userRepository, IAuthenticator authenticator) {
        this.userRepository = userRepository;
        this.authenticator = authenticator;
    }


    public void login(String username, AuthToken authToken) throws AuthenticationException {
        IUser user = userRepository.findUser(username);

        if (user == null) {
            throw new UserNotFoundException();
        }

        if (user.isAuthenticated()) {
            throw new AlreadyLoggedInException();
        }

        boolean authenticated = authenticator.authenticate(user, authToken);

        if (!authenticated) {
            throw new InvalidCredentialsException();
        }

        user.setAuthenticated(true);
        currentUser = user;
    }


    public void logout(IUser user) throws AuthenticationException {
        if (!user.isAuthenticated()) {
            throw new NotAuthenticatedException();
        }

        user.setAuthenticated(false);
    }


    public IUser getCurrentUser() {
        return currentUser;
    }


    public void setUserRepository(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public void setAuthenticator(IAuthenticator authenticator) {
        this.authenticator = authenticator;
    }
}
