package cz.cvut.fit.spravaprojektu.usermanager;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public interface IPermissionChecker {

    public boolean isAllowed(IUser user, String permission);
}
