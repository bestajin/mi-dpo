package cz.cvut.fit.spravaprojektu.usermanager;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public interface IRoleManager {

    public boolean hasRole(IUser user, String roleIdentifier);
}
