package cz.cvut.fit.spravaprojektu.usermanager;


import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;


/**
 * Rozhraní pro různé typy autentizace uživatelů
 *
 * @author bestajin@fit.cvut.cz
 */
public interface IAuthenticator {

    /**
     * Provede autentizaci uživatele pomocí přihlašovacích údajů. Pokud je
     * autentizace úspěšná, vrací
     * <code>true</code>, jinak
     * <code>false</code>.
     *
     * @param user
     * @param authToken
     * @return true, pokud je autentizace úspěšná, jinak false.
     */
    public boolean authenticate(IUser user, AuthToken authToken);
}
