package cz.cvut.fit.spravaprojektu.usermanager;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public interface IPasswordRepository {

    /**
     * Vrátí heslo pro uživatele, pokud uživatel není nalezen, vrátí
     * <code>null</code>
     *
     * @param username
     * @return
     */
    public String find(String username);
}
