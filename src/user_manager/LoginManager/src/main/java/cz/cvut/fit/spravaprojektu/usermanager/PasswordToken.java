package cz.cvut.fit.spravaprojektu.usermanager;


/**
 * Přihlašovací token pomocí hesla
 *
 * @author jilly
 */
public class PasswordToken implements AuthToken {

    private String password;


    public PasswordToken(String password) {
        this.password = password;
    }


    /**
     * Vrátí heslo
     *
     * @return
     */
    public Object getCredentials() {
        return password;
    }
}
