/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.mi_dpo_2013_skoulmir_module;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 *
 * @author skoulmir
 */
public class Sync {

    private List<Conflict> conflicts = new ArrayList<Conflict>();
    private static Sync instance = new Sync();
    private DBConnector localDB;
    private DBConnector masterDB;

    public static Sync getInstance(){
        return instance;
    } 

    public Sync() {
        localDB = new DBConnector();
        localDB.addtask(new Task(new Date(), new Date(), 13, "Konflikt", "Local", null, null));
        masterDB = new DBConnector();
        masterDB.addtask(new Task(new Date(), new Date(), 12, "Konflikt", "Master", null, null));
    }
    
    
    
    
    public List<Conflict> getConflicts() {
        return new ArrayList<>(conflicts);
    }

    public void resolve(boolean override, Conflict conflict) {
        int revision = Math.max(conflict.getMaster().getRevision(), 
                conflict.getLocal().getRevision());
        if (override) {
            conflict.getMaster().removeFrom(masterDB);
            conflict.getLocal().setSynchronized(revision);
            conflict.getLocal().addTo(masterDB);
        } else {
            conflict.getLocal().removeFrom(localDB);
            conflict.getMaster().setSynchronized(revision);
            conflict.getMaster().addTo(localDB);
        }
        conflicts.remove(conflict);
    }

    public void setLocal(DBConnector local) {
        this.localDB = local;
    }
    
    public void setMaster(DBConnector master) {
        this.masterDB = master;
    }

    public boolean synchronize() {
        boolean result = false;
        List<Task> localTasks = localDB.getTasks(new TaskFilter());
        List<Task> masterTasks = masterDB.getTasks(new TaskFilter());
        Collections.sort(localTasks);
        Collections.sort(masterTasks);
        result = result || synchronize(
                localTasks.toArray(),
                masterTasks.toArray());
        List<Project> localTProject = localDB.getProject(new ProjectFilter());
        List<Project> masterProject = masterDB.getProject(new ProjectFilter());
        Collections.sort(localTProject);
        Collections.sort(masterProject);
        result = result || synchronize(
                localTProject.toArray(),
                masterProject.toArray());
        return !result;
    }
    private boolean synchronize(Object[] localCollection,
            Object[] masterCollection) {
        boolean result = false;
        int m = masterCollection.length - 1;
        int l = localCollection.length - 1;
        while (m >= 0 && l >= 0) {
            ITask masterTask = (ITask)masterCollection[m];
            ITask localTask = (ITask)localCollection[l];
            int cmp = masterTask.compareTo(localTask);
            //tasks are same
            if (cmp == 0) {
                if (masterTask.getRevision() != localTask.getRevision()
                        || masterTask.isEdited() || localTask.isEdited()) {
                    conflicts.add(new Conflict(masterTask, localTask));
                    result = true;
                }
                m--;
                l--;
                //tasks are differant
            } else if (cmp > 0) {
                masterTask.setSynchronized(masterTask.getRevision());
                masterTask.addTo(localDB);
                m--;
            } else {
                localTask.setSynchronized(localTask.getRevision());
                localTask.addTo(masterDB);
                l--;
            }
        }
        while (m >= 0) {
            ITask masterTask = (ITask) masterCollection[m];
            masterTask.setSynchronized(masterTask.getRevision());
            masterTask.addTo(localDB);
            m--;
        }
        while (l >= 0) {
            ITask localTask = (ITask) localCollection[l];
            localTask.setSynchronized(localTask.getRevision());
            localTask.addTo(masterDB);
            l--;
        }
        return result;
    }
}
