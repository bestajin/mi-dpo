/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.mi_dpo_2013_skoulmir_dummy;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;

/**
 *
 * @author karel-pc
 */
public interface ITaskVisitor {
    public void visit(Project project);
    public void visit(Task task);
}
