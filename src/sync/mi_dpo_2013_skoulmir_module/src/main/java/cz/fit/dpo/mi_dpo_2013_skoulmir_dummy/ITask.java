/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.mi_dpo_2013_skoulmir_dummy;

import cz.cvut.fit.spravaprojektu.usermanager.UserModel;




/**
 *
 * @author mini
 */
public abstract class ITask implements Comparable<ITask> {
    protected boolean edited;
    protected int revision;
    protected Long id;
    protected String name;
    protected String description;
    protected UserModel owner;
    protected State state;

    public ITask(String name, String description, UserModel owner, State state) {
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.state = state;
        this.edited = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.edited = true;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        this.edited = true;
    }

    public UserModel getOwner() {
        return owner;
    }

    public void setOwner(UserModel owner) {
        this.owner = owner;
        this.edited = true;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
        this.edited = true;
    }

    public int getRevision() {
        return revision;
    }
    public boolean isEdited(){
        return edited;
    }
    abstract public void addTo(DBConnector db);
    abstract public void removeFrom(DBConnector db);
    public void setEdited(){
        edited = true;
    }
    public void setSynchronized(int revision){
        this.revision = revision;
        edited = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    
    
    public abstract void acceptVisitor(ITaskVisitor taskVisitor);
}
