/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.mi_dpo_2013_skoulmir_dummy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mini
 */
public class DBConnector {
    List<Project> projects = new ArrayList<Project>();
    List<Task> tasks = new ArrayList<Task>();
    
    public void addProject(Project project){
        projects.add(project);
    }
    public void addtask(Task task){
        tasks.add(task);
    }
    public void removeProject(Project project){
        projects.remove(project);
    }
    public void removeTask(Task task){
        tasks.remove(task);
    }
    public List<Project> getProject(ProjectFilter filter){
        List<Project> result = new ArrayList<Project>(projects);
        Collections.sort(result);
        return result;
    }
    public List<Task>getTasks(TaskFilter filter){
        List<Task> result = new ArrayList<Task>(tasks);
        Collections.sort(result);
        return result;
    }
}
