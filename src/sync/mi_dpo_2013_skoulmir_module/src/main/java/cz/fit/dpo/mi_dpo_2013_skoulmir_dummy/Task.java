/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.mi_dpo_2013_skoulmir_dummy;

import cz.cvut.fit.spravaprojektu.usermanager.UserModel;
import java.util.Date;


/**
 *
 * @author mini
 */
public class Task extends ITask {
    private Date start;
    private Date finish;
    private Number costs; 
    
    public Task(Date start, Date finish, Number costs, String name, String description, UserModel owner, State state) {
        super(name, description, owner, state);
        this.start = start;
        this.finish = finish;
        this.costs = costs;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
        this.edited = true;
    }

    public Date getFinish() {
        return finish;
    }

    public void setFinish(Date finish) {
        this.finish = finish;
        this.edited = true;
    }

    public Number getCosts() {
        return costs;
    }

    public void setCosts(Number costs) {
        this.costs = costs;
        this.edited = true;
    }
    
    @Override
    public void addTo(DBConnector db){
        db.addtask(this);
    }
    @Override
    public void removeFrom(DBConnector db){
        db.removeTask(this);
    }
    @Override
    public int compareTo(ITask task) {
        if(task instanceof Task){
            return this.name.compareTo(((Task)task).name);
        }
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void acceptVisitor(ITaskVisitor taskVisitor) {
        taskVisitor.visit(this);
    }
}
