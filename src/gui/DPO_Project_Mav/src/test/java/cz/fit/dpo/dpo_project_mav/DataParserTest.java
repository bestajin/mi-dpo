/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav;

import cz.fit.dpo.dpo_project_mav.controls.ITaskPanelParser;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ITaskPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ProjectPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.TaskPanel;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.util.ArrayList;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author Radek
 */
public class DataParserTest {
    
    @Test
	public void testParseTask() {
        ITaskPanelParser parser = new ITaskPanelParser();
        
        TaskPanel tp = new TaskPanel(null, false);
        tp.getNameTextField().setText("Name");
        tp.getDescriptionTextField().setText("Desc");
        tp.getStartDateCalendar().setDate(new Date(0));
        tp.getFinishDateCalendar().setDate(new Date(10));
        tp.getCostsTextField().setText("100");
        Task res = (Task) parser.parseITaskFromPanel(tp);
        
        assertEquals(res.getName(), "Name");
        assertEquals(res.getDescription(), "Desc");
        assertEquals(res.getStart(), new Date(0));
        assertEquals(res.getFinish(), new Date(10));
        assertTrue(res.getCosts().intValue() == 100);
    }
    
    @Test
	public void testParseProject() {
        ITaskPanelParser parser = new ITaskPanelParser();
        
        ProjectPanel pp = new ProjectPanel(null, false);
        pp.getNameTextField().setText("Name");
        pp.getDescriptionTextField().setText("Desc");
        
        ArrayList<ITaskPanel> res = pp.getProjects();
        ProjectPanel pp2 = new ProjectPanel(null, false);
        pp2.getNameTextField().setText("Name2");
        pp2.getDescriptionTextField().setText("Desc2");
        res.add(pp2);
        
        TaskPanel tp = new TaskPanel(null, false);
        tp.getNameTextField().setText("Name");
        tp.getDescriptionTextField().setText("Desc");
        tp.getStartDateCalendar().setDate(new Date(0));
        tp.getFinishDateCalendar().setDate(new Date(10));
        tp.getCostsTextField().setText("100");
        res.add(tp);
        
        Project pRes = (Project) parser.parseITaskFromPanel(pp);
        
        
        assertEquals(pRes.getName(), "Name");
        assertEquals(pRes.getDescription(), "Desc");
        assertTrue(pRes.getProjects().size() == 2);
        
        Project ptmp = (Project) pRes.getProjects().toArray()[0];
        assertEquals(ptmp.getName(), "Name2");
        assertEquals(ptmp.getDescription(), "Desc2");
        assertTrue(ptmp.getProjects().isEmpty());
        
        Task ttmp = (Task) pRes.getProjects().toArray()[1];
        assertEquals(ttmp.getName(), "Name");
        assertEquals(ttmp.getDescription(), "Desc");
        assertEquals(ttmp.getStart(), new Date(0));
        assertEquals(ttmp.getFinish(), new Date(10));
        assertTrue(ttmp.getCosts().intValue() == 100);
    }
}
