/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics.visitors;

import cz.fit.dpo.dpo_project_mav.graphics.panels.ProjectPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.TaskPanel;

/**
 *
 * @author Radek
 */
public class PanelDataCopyVisitor {
    public ProjectPanel copy(ProjectPanel project){
        ProjectPanel panel = new ProjectPanel(null, project.isReadOnly());
        panel.copyOnlyData(project);
        return panel;
    }
    
    public TaskPanel copy(TaskPanel task){
        TaskPanel panel = new TaskPanel(null, task.isReadOnly());
        panel.copyOnlyData(task);
        return panel;
    }
}
