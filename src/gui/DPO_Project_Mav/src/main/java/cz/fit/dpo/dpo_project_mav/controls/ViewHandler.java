/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.controls;

import cz.fit.dpo.dpo_project_mav.data.DataHandler;
import cz.fit.dpo.dpo_project_mav.graphics.ConflictDialog;
import cz.fit.dpo.dpo_project_mav.graphics.ListTaskDialog;
import cz.fit.dpo.dpo_project_mav.graphics.visitors.PanelNewDialogVisitor;
import cz.fit.dpo.dpo_project_mav.graphics.LoginDialog;
import cz.fit.dpo.dpo_project_mav.graphics.TaskPanelDialog;
import cz.fit.dpo.dpo_project_mav.graphics.panels.StartPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.TaskPanel;
import cz.fit.dpo.dpo_project_mav.graphics.ViewFrame;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ITaskPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ListITaskPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ProjectPanel;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import cz.fit.dpo.mi_dpo_2013_skoulmir_module.Sync;
import javax.swing.JOptionPane;

/**
 *
 * @author Radek
 */
public final class ViewHandler {

    private ViewFrame view;
    private ITaskPanelParser parser;
    private boolean loginSuccesfull = false;
    private LoginDialog loginDialog;

    public ViewHandler() {
        loginDialog = new LoginDialog(this);
        if (loginSuccesfull == false) {
            return;
        }
        view = new ViewFrame();
        parser = new ITaskPanelParser();
        showStartPanel();
    }

    public boolean loginUser(String login, String passwd) {
        if (UserControl.getInstance().existUser(login, passwd)) {
            loginSuccesfull = true;
            return true;
        }
        return false;
    }

    public void showStartPanel() {
        view.showPanel(new StartPanel(this));
    }

    public void showNewTaskDialog(TaskPanel tp) {
        if (tp == null) {
            tp = new TaskPanel(this, false);
        }
        TaskPanelDialog viewDialog = new TaskPanelDialog(null, "New Task", true, tp);
        if (tp.isForSerialize()) {
            Task task = (Task) parser.parseITaskFromPanel(tp);
            DataHandler.genInstance().saveITask(task);
        }
    }
    
    public void showNewTaskDialogWithinDialog(TaskPanel tp) {
        if (tp == null) {
            tp = new TaskPanel(this, false);
        }
        TaskPanelDialog viewDialog = new TaskPanelDialog(null, "New Task", true, tp);
    }

    public void showNewProjectDialog(ProjectPanel pp) {
        if (pp == null) {
            pp = new ProjectPanel(this, false);
        }
        TaskPanelDialog viewDialog = new TaskPanelDialog(null, "New Project", true, pp);
        if (pp.isForSerialize()) {
            Project project = (Project) parser.parseITaskFromPanel(pp);
            DataHandler.genInstance().saveITask(project);
        }
    }
    
    public void showNewProjectDialogWithinDialog(ProjectPanel pp) {
        if (pp == null) {
            pp = new ProjectPanel(this, false);
        }
        TaskPanelDialog viewDialog = new TaskPanelDialog(null, "New Project", true, pp);
    }

    public void showITaskEditDialogWithinNewTaskDialog(ITaskPanel panel) {
        panel.accept(new PanelNewDialogVisitor());
    }

    public void showITaskEditDialog(ITaskPanel panel) {
        TaskPanelDialog dialog = new TaskPanelDialog(null, "Edit", true, panel);
    }

    public void showWrongInput() {
        JOptionPane.showMessageDialog(null, "Wrong input!");
    }

    public void showListITasks() {
        ListTaskDialog dialog = new ListTaskDialog(null, "List of ITasks", true, new ListITaskPanel(this));
    }

    public void showListUsers() {
    }

    public void exitApplication() {
        view.dispose();
    }
    
    public void synchronize(){
        if(Sync.getInstance().synchronize() == true){
            JOptionPane.showMessageDialog(null, "Synchronization successfull.");
        }else{
            JOptionPane.showMessageDialog(null, "Synchronization failed.");
            ConflictDialog dialog = new ConflictDialog(null, "Conflict Dialog", true, this);
        }
    }

    public LoginDialog getLoginDialog() {
        return loginDialog;
    }
    
    
}
