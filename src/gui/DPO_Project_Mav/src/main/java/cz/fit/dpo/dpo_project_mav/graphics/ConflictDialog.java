/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics;

import cz.fit.dpo.dpo_project_mav.controls.ViewHandler;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ITaskPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ProjectPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.TaskPanel;
import cz.fit.dpo.mi_dpo_2013_skoulmir_module.Conflict;
import cz.fit.dpo.mi_dpo_2013_skoulmir_module.Sync;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Radek
 */
public class ConflictDialog extends JDialog{

    private Iterator<Conflict> conflictIterator;
    private JPanel panel;
    private ViewHandler viewHandler;
    private int counter = 0;
    
    public ConflictDialog(Dialog owner, String title, boolean modal, ViewHandler viewHandler) {
        super(owner, title, modal);
        this.viewHandler = viewHandler;
        
        conflictIterator = Sync.getInstance().getConflicts().iterator();
        panel = new JPanel();
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        add(panel);
        addConflict();
        setLocation(350, 200);
        setVisible(true);
    }
    
    final public void addConflict(){
        if(conflictIterator.hasNext() == false){
            JOptionPane.showMessageDialog(null, "OK");
            this.dispose();
            return;
        }
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        JLabel label = new JLabel("Conflict " + ++counter);
        panel.add(label);
        
        label = new JLabel("Local: ");
        panel.add(label);
        final Conflict c = conflictIterator.next();
        JPanel iTaskConflictPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        label = new JLabel("Revision: ");
        iTaskConflictPanel.add(label);
        label = new JLabel(Integer.toString(c.getLocal().getRevision()));
        iTaskConflictPanel.add(label);
        
        JButton showButton = new JButton("Show");
        showButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //v tuhle chvili by bylo potreba pouzit dekorator na ITask a pote visitor, aby mi vracel ITaskPanel podle sveho typu
                //z duvodu casove tisne provedu rychlejsi reseni instanceof
                ITaskPanel panel;
                if(c.getLocal() instanceof Task){
                    panel = new TaskPanel(viewHandler, true);
                }else{
                    panel = new ProjectPanel(viewHandler, true);
                }
                panel.setITaskData(c.getLocal());
                viewHandler.showITaskEditDialog(panel);
            }
        });
        iTaskConflictPanel.add(showButton);
        JButton keepButton = new JButton("Keep");
        keepButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Sync.getInstance().resolve(false, c);
                addConflict();
            }
        });
        iTaskConflictPanel.add(keepButton);
        panel.add(iTaskConflictPanel);
        
        label = new JLabel("Master: ");
        panel.add(label);
        iTaskConflictPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        label = new JLabel("Revision: ");
        iTaskConflictPanel.add(label);
        label = new JLabel(Integer.toString(c.getMaster().getRevision()));
        iTaskConflictPanel.add(label);
        
        showButton = new JButton("Show");
        showButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //v tuhle chvili by bylo potreba pouzit dekorator na ITask a pote visitor, aby mi vracel ITaskPanel podle sveho typu
                //z duvodu casove tisne provedu rychlejsi reseni instanceof
                ITaskPanel panel;
                if(c.getLocal() instanceof Task){
                    panel = new TaskPanel(viewHandler, true);
                }else{
                    panel = new ProjectPanel(viewHandler, true);
                }
                panel.setITaskData(c.getMaster());
                viewHandler.showITaskEditDialog(panel);
            }
        });
        iTaskConflictPanel.add(showButton);
        keepButton = new JButton("Keep");
        keepButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Sync.getInstance().resolve(true, c);
                addConflict();
            }
        });
        iTaskConflictPanel.add(keepButton);
        panel.add(iTaskConflictPanel);
        panel.invalidate();
        invalidate();
        pack();
    }
    
    
}
