/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics;

import cz.fit.dpo.dpo_project_mav.controls.ViewHandler;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Radek
 */
public class LoginDialog extends JDialog{

    private JTextField inputName;
    private JPasswordField inputPasswd;
    private final int INPUT_WIDTH = 20;
    private final int PASSWD_WIDTH = 18;
    private ViewHandler viewHandler;
    JButton loginButton;
    
    public LoginDialog(final ViewHandler viewHandler) {
        super((Frame)null, "Login Dialog", true);
        
        this.viewHandler = viewHandler;
        setLocation(350, 100);
        setSize(350, 150);
        setLayout(new FlowLayout(FlowLayout.CENTER));
        JPanel rowLogin = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel name = new JLabel("Name:");
        rowLogin.add(name);
        inputName = new JTextField(INPUT_WIDTH);
        rowLogin.add(inputName);
        add(rowLogin);
        
        JPanel rowPasswd = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel passwd = new JLabel("Password:");
        rowPasswd.add(passwd);
        inputPasswd = new JPasswordField(PASSWD_WIDTH);
        rowPasswd.add(inputPasswd);
        add(rowPasswd);
        
        JPanel rowButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        rowButtons.add(exitButton);
        loginButton = new JButton("Login");
        loginButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(viewHandler.loginUser(inputName.getText(), new String(inputPasswd.getPassword())) == true)
                    dispose();
                else
                    JOptionPane.showMessageDialog(null, "Incorrect login.");
            }
        });
        rowButtons.add(loginButton);
        add(rowButtons);
        setVisible(true);
    }

    public JButton getLoginButton() {
        return loginButton;
    }
    
    
}
