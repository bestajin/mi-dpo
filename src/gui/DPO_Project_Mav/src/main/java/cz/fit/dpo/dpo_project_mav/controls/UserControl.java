/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.controls;

import cz.cvut.fit.spravaprojektu.usermanager.IUser;
import cz.cvut.fit.spravaprojektu.usermanager.PasswordToken;
import cz.cvut.fit.spravaprojektu.usermanager.UserManager;
import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;
import cz.cvut.fit.spravaprojektu.usermanager.factory.stupid.StupidUserManagerFactory;
import cz.cvut.fit.spravaprojektu.usermanager.UserModel;
//import cz.fit.dpo.dpo_project_mav.notMyBusiness.UserModel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Radek
 */
public class UserControl {
    
    private IUser loggedUser;
    private static UserControl instance;
    private UserManager userManager;
    
    private UserControl() {
        // TODO: mel by byt predavan konstruktorem
        userManager = new StupidUserManagerFactory().createUserManager();
    }
    
    public static UserControl getInstance(){
        if(instance == null)
            instance = new UserControl();
        return instance;
    }
    
    public boolean existUser(String username, String passwd){
        try {
            // TODO: nacitat opravdove heslo z GUI
            userManager.login(username, new PasswordToken(passwd));
            loggedUser = userManager.getCurrentUser();
            
            return true;
        } catch (AuthenticationException ex) {
            
            return false;
        }
    }
    
    public IUser getUser() {
        return loggedUser;
    }
}
