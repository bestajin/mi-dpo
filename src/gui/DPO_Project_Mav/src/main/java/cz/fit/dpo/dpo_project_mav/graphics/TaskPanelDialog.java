/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics;

import cz.fit.dpo.dpo_project_mav.graphics.panels.ITaskPanel;
import java.awt.Frame;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 *
 * @author Radek
 */
public class TaskPanelDialog extends JDialog{

    JPanel panel;
    
    public TaskPanelDialog(Frame owner, String title, boolean modal, ITaskPanel panel) {
        super(owner, title, modal);
        this.panel = panel;
        
        setLocation(400, 200);
        panel.setParentDialog(this);
        add(panel);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
    }
    
    public void updateScene(){
        invalidate();
        pack();
    }
    
}
