/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics.panels;

import cz.fit.dpo.dpo_project_mav.controls.visitors.PanelDataParserVisitor;
import cz.fit.dpo.dpo_project_mav.controls.ViewHandler;
import cz.fit.dpo.dpo_project_mav.graphics.visitors.PanelDataCopyVisitor;
import cz.fit.dpo.dpo_project_mav.graphics.visitors.PanelNewDialogVisitor;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Radek
 */
public class ProjectPanel extends ITaskPanel {

    private ArrayList<ITaskPanel> projects;
    private JPanel projectsPanel;

    public ProjectPanel(final ViewHandler viewHandler, boolean readOnly) {
        super(viewHandler, readOnly);
        projects = new ArrayList<ITaskPanel>();

        projectsPanel = new JPanel();
        projectsPanel.setLayout(new BoxLayout(projectsPanel, BoxLayout.Y_AXIS));
        add(projectsPanel);

        JPanel row = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton newTask = new JButton("New Task...");
        newTask.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TaskPanel tp = new TaskPanel(viewHandler, false);
                projects.add(tp);
                viewHandler.showNewTaskDialogWithinDialog(tp);
                if (tp.isForSerialize() == false) {
                    projects.remove(projects.size() - 1);
                } else {
                    addITaskInfoToProjects(tp, projects.size() - 1);
                    parentDialog.updateScene();
                }
            }
        });
        row.add(newTask);
        JButton newProject = new JButton("New Project...");
        newProject.setEnabled(!readOnly);
        newProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProjectPanel pp = new ProjectPanel(viewHandler, false);
                projects.add(pp);
                viewHandler.showNewProjectDialogWithinDialog(pp);
                if (pp.isForSerialize() == false) {
                    projects.remove(projects.size() - 1);
                } else {
                    addITaskInfoToProjects(pp, projects.size() - 1);
                    parentDialog.updateScene();
                }
            }
        });
        row.add(newProject);
        add(row);

        addButtons();
    }

    private void addITaskInfoToProjects(final ITaskPanel panel, final int taskID) {
        JPanel row = new JPanel(new FlowLayout(FlowLayout.LEFT));

        JLabel label = new JLabel("Name: ");
        row.add(label);
        label = new JLabel(panel.getAttrName());
        row.add(label);
        label = new JLabel("Description: ");
        row.add(label);
        label = new JLabel(panel.getAttrDescription());
        row.add(label);
        JButton editButton = new JButton("Edit");
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ITaskPanel panelCopy = panel.accept(new PanelDataCopyVisitor());
                viewHandler.showITaskEditDialogWithinNewTaskDialog(panel);
                if (panel.isForSerialize() == false) {
                    panel.copyOnlyData(panelCopy);
                } else {
                    updateITaskInfoProjects(taskID);
                }
                parentDialog.updateScene();
            }
        });
        row.add(editButton);

        JButton deleteButton = new JButton("Delete");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                projects.remove(taskID);
                projectsPanel.remove(taskID);
                parentDialog.updateScene();
            }
        });
        row.add(deleteButton);
        projectsPanel.add(row);
    }

    private void updateITaskInfoProjects(int taskID) {
        JPanel row = (JPanel)projectsPanel.getComponent(taskID);
        ITaskPanel panel = projects.get(taskID);
        JLabel label = (JLabel)row.getComponent(1);
        label.setText(panel.getAttrName());
        label = (JLabel)row.getComponent(3);
        label.setText(panel.getAttrDescription());
        projectsPanel.invalidate();
    }
    
    private void updateITaskInfoProjectsAll() {
        this.projectsPanel.removeAll();
        int id = 0;
        for (ITaskPanel iTaskPanel : projects) {
            addITaskInfoToProjects(iTaskPanel, id++);
        }
    }

    public void copyOnlyData(ProjectPanel panelOrig) {
        super.copyOnlyData(panelOrig);
        this.projects = panelOrig.projects;
    }
    
    public ArrayList<ITaskPanel> getProjects() {
        return projects;
    }

    @Override
    public boolean checkData() {
        return super.isFilledEverything() && (!projects.isEmpty());
    }

    @Override
    public void setITaskData(ITask iTask) {
        super.setITaskData(iTask);
        
        Project project = (Project) iTask;
        ArrayList<ITaskPanel> res = new ArrayList<ITaskPanel>();
        //v tuhle chvili by bylo potreba pouzit dekorator na ITask a pote visitor, aby mi vracel ITaskPanel podle sveho typu
        //z duvodu casove tisne provedu rychlejsi reseni instanceof
        ITaskPanel itp;
        for (ITask iTask1 : project.getProjects()) {
            if(iTask1 instanceof ITask){
                itp = new TaskPanel(viewHandler, readOnly);
            }else{
                itp = new ProjectPanel(viewHandler, readOnly);
            }
            itp.setITaskData(iTask1);
            res.add(itp);
        }
        this.projects = res;
        updateITaskInfoProjectsAll();
    }

    
    
    @Override
    public void accept(PanelNewDialogVisitor v) {
        v.showNewDialog(this, viewHandler);
    }

    @Override
    public ITask parse(PanelDataParserVisitor v) {
        return v.process(this);
    }

    @Override
    public ITaskPanel accept(PanelDataCopyVisitor v) {
        return v.copy(this);
    }
}
