/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.controls.visitors;

import cz.fit.dpo.dpo_project_mav.controls.UserControl;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ITaskPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ProjectPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.TaskPanel;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.State;
//import cz.fit.dpo.dpo_project_mav.notMyBusiness.State;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.util.LinkedList;

/**
 *
 * @author Radek
 */
public class PanelDataParserVisitor {

    public Project process(ProjectPanel project) {
        Project res;
        LinkedList<ITask> projects = new LinkedList<ITask>();
        for (ITaskPanel iTaskPanel : project.getProjects()) {
            projects.add(iTaskPanel.parse(this));
        }
        res = new Project(projects, project.getAttrName(), project.getAttrDescription(), 
                null, new State("New"));
        return res;
    }
    
    public Task process(TaskPanel task){
        Task res;
        res = new Task(task.getStartDate(), task.getFinishDate(), Double.parseDouble(task.getCosts()), 
                task.getAttrName(), task.getAttrDescription(), null, new State("New"));
        return res;
    }
}
