/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics.panels;

import com.toedter.calendar.JCalendar;
import cz.fit.dpo.dpo_project_mav.controls.ViewHandler;
import cz.fit.dpo.dpo_project_mav.controls.visitors.PanelDataParserVisitor;
import cz.fit.dpo.dpo_project_mav.graphics.visitors.PanelDataCopyVisitor;
import cz.fit.dpo.dpo_project_mav.graphics.visitors.PanelNewDialogVisitor;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.awt.FlowLayout;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Radek
 */
public class TaskPanel extends ITaskPanel{
    private JCalendar startDate;
    private JCalendar finishDate;
    private JTextField costs;

    
    public TaskPanel(final ViewHandler viewHandler, boolean readOnly) {
        super(viewHandler, readOnly);      
               
        JPanel row = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel label = new JLabel("Start:");
        startDate = new JCalendar();
        startDate.setEnabled(!readOnly);
        row.add(label);
        row.add(startDate);
        add(row);
        
        row = new JPanel(new FlowLayout(FlowLayout.LEFT));
        label = new JLabel("Finish:");
        finishDate = new JCalendar();
        finishDate.setEnabled(!readOnly);
        row.add(label);
        row.add(finishDate);
        add(row);
        
        row = new JPanel(new FlowLayout(FlowLayout.LEFT));
        label = new JLabel("Costs:");
        costs = new JTextField(10);
        costs.setEnabled(!readOnly);
        row.add(label);
        row.add(costs);
        add(row);
        
        addButtons();
    }

     
    public Date getStartDate(){
        return startDate.getDate();
    }
    
    public Date getFinishDate(){
        return finishDate.getDate();
    }
    
    public String getCosts(){
        return costs.getText();
    }

    public void copyOnlyData(TaskPanel panelOrig) {
        super.copyOnlyData(panelOrig);
        this.startDate = panelOrig.startDate;
        this.finishDate = panelOrig.finishDate;
    }

    @Override
    public boolean checkData() {
        if(costs.getText().equals("") == false){
            try{
                Double.parseDouble(costs.getText());
            }catch(NumberFormatException ex){
                return false;
            }
            return super.isFilledEverything();
        }
        return false;
    }

    @Override
    public void setITaskData(ITask iTask) {
        super.setITaskData(iTask); 
        
        Task task = (Task) iTask;
        this.startDate.setDate(task.getStart());
        this.finishDate.setDate(task.getFinish());
        this.costs.setText(task.getCosts().toString());
    }
    
   
    

    @Override
    public void accept(PanelNewDialogVisitor v) {
        v.showNewDialog(this, viewHandler);
    }
    
    @Override
    public ITask parse(PanelDataParserVisitor v) {
        return v.process(this);
    }

    @Override
    public ITaskPanel accept(PanelDataCopyVisitor v) {
        return v.copy(this);
    }

    public JCalendar getStartDateCalendar() {
        return startDate;
    }

    public JCalendar getFinishDateCalendar() {
        return finishDate;
    }

    public JTextField getCostsTextField() {
        return costs;
    }

    
    
}
