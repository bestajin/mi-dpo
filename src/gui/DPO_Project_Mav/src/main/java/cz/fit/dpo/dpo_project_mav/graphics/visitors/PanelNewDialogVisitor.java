/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics.visitors;

import cz.fit.dpo.dpo_project_mav.controls.ViewHandler;
import cz.fit.dpo.dpo_project_mav.graphics.TaskPanelDialog;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ProjectPanel;
import cz.fit.dpo.dpo_project_mav.graphics.panels.TaskPanel;

/**
 *
 * @author Radek
 */
public class PanelNewDialogVisitor {
            
    public void showNewDialog(ProjectPanel project, ViewHandler handler){
        new TaskPanelDialog(null, "New Project", true, project);
    }
    
    public void showNewDialog(TaskPanel task, ViewHandler handler){
        new TaskPanelDialog(null, "New Task", true, task);
    }
    
}
