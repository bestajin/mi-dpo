/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics.panels;

import cz.fit.dpo.dpo_project_mav.controls.ViewHandler;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Radek
 */
public class StartPanel extends JPanel{
    private ViewHandler viewHandler;
    
    public StartPanel(final ViewHandler viewHandler) {
        this.viewHandler = viewHandler;
        
        setLayout(new GridLayout(0, 1));
        JButton newTaskButton = new JButton("New Task");
        newTaskButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                viewHandler.showNewTaskDialog(null);
            }
        });
        add(newTaskButton);
        
        JButton newProjectButton = new JButton("New Project");
        newProjectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                viewHandler.showNewProjectDialog(null);
            }
        });
        add(newProjectButton);
        
        JButton listProjectsButton = new JButton("List Projects");
        listProjectsButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                viewHandler.showListITasks();
            }
        });
        add(listProjectsButton);
        
//        JButton listUsersButton = new JButton("List Users");
//        listUsersButton.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                viewHandler.showListUsers();
//            }
//        });
//        add(listUsersButton);
        
        JButton synchButton = new JButton("Synchronize");
        synchButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                viewHandler.synchronize();
            }
        });
        add(synchButton);
        
        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                viewHandler.exitApplication();
            }
        });
        add(exitButton);
    }
}
