/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.data;


import cz.fit.dpo.filters.ProjectFilter;
import cz.fit.dpo.filters.TaskFilter;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import cz.fit.dpo.modules.dbConnector.SimpleDbConnector;
import cz.fit.dpo.modules.taskManager.ITaskManager;
import cz.fit.dpo.modules.taskManager.ITaskManagerFactory;
import cz.fit.dpo.modules.taskManager.TaskManagerFactory;
import java.util.List;

/**
 *
 * @author Radek
 */
public class DataHandler {

    private static DataHandler instance;
    
    private ITaskManager taskManager;
    
    private DataHandler(ITaskManager taskManager) {
        this.taskManager = taskManager;
    }
    
    public static DataHandler genInstance(){
        if(instance == null) {
            ITaskManagerFactory taskManagerFactory = new TaskManagerFactory();
            ITaskManager taskManager = taskManagerFactory.createTaskManager(new SimpleDbConnector());
            instance = new DataHandler(taskManager);
        }
        
        return instance;
    }
    
    public void saveITask(ITask task){
        taskManager.add(task);
    }
    
    public void editITask(ITask task){
        taskManager.edit(task);
    }
    
    public void removeITask(ITask task){
        taskManager.remove(task);
    }
    
    public List<Task> getTasks(){
        return taskManager.getTasks(new TaskFilter());
    }
    
    public List<Project> getProjects(){
        return taskManager.getProjects(new ProjectFilter());
    }
}
