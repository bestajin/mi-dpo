/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics.panels;

import cz.fit.dpo.dpo_project_mav.controls.ITaskPanelParser;
import cz.fit.dpo.dpo_project_mav.controls.ViewHandler;
import cz.fit.dpo.dpo_project_mav.data.DataHandler;
import cz.fit.dpo.dpo_project_mav.graphics.ListTaskDialog;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Radek
 */
public class ListITaskPanel extends JPanel {

    private ViewHandler viewHandler;
    private ITaskPanelParser parser;
    private JPanel taskPanel;
    private JPanel projectPanel;
    private ListTaskDialog owner;
    
    public ListITaskPanel(final ViewHandler viewHandler) {
        this.viewHandler = viewHandler;
        
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        parser = new ITaskPanelParser();
        taskPanel = new JPanel();
        taskPanel.setLayout(new BoxLayout(taskPanel, BoxLayout.Y_AXIS));
        projectPanel = new JPanel();
        projectPanel.setLayout(new BoxLayout(projectPanel, BoxLayout.Y_AXIS));
        
        JPanel row = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel label = new JLabel("Tasks:");
        row.add(label);
        add(row);
        add(taskPanel);
        
        row = new JPanel(new FlowLayout(FlowLayout.CENTER));
        label = new JLabel("Projects:");
        row.add(label);
        add(row);
        add(projectPanel);
        updateTaskRows();
        
        row = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton close = new JButton("Close");
        close.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                owner.dispose();
            }
        });
        row.add(close);
        add(row);
    }
    
    public final void updateTaskRows(){
        taskPanel.removeAll();
        projectPanel.removeAll();
        
        List<Task> tasks = DataHandler.genInstance().getTasks();
        List<Project> projects = DataHandler.genInstance().getProjects();
        
        JPanel row;
        JLabel label;
        for (final Task task : tasks) {
            row = new JPanel(new FlowLayout(FlowLayout.LEFT));

            label = new JLabel("Name: ");
            row.add(label);
            label = new JLabel(task.getName());
            row.add(label);
            label = new JLabel("Description: ");
            row.add(label);
            label = new JLabel(task.getDescription());
            row.add(label);
            JButton editButton = new JButton("Edit");
            editButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TaskPanel panel = new TaskPanel(viewHandler, false);
                    panel.setITaskData(task);
                    viewHandler.showITaskEditDialog(panel);
                    if (panel.isForSerialize() != false) {
                        Task tNew = (Task) parser.parseITaskFromPanel(panel);
                        DataHandler.genInstance().editITask(tNew);
                        updateTaskRows();
                        owner.updateScene();
                    } 
                }
            });
            row.add(editButton);

            JButton deleteButton = new JButton("Delete");
            deleteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataHandler.genInstance().removeITask(task);
                    updateTaskRows();
                    owner.updateScene();
                }
            });
            row.add(deleteButton);
            taskPanel.add(row);
        }
        for (final Project project : projects) {
            row = new JPanel(new FlowLayout(FlowLayout.LEFT));

            label = new JLabel("Name: ");
            row.add(label);
            label = new JLabel(project.getName());
            row.add(label);
            label = new JLabel("Description: ");
            row.add(label);
            label = new JLabel(project.getDescription());
            row.add(label);
            JButton editButton = new JButton("Edit");
            editButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ProjectPanel panel = new ProjectPanel(viewHandler, false);
                    panel.setITaskData(project);
                    viewHandler.showITaskEditDialog(panel);
                    if (panel.isForSerialize() != false) {
                        Task tNew = (Task) parser.parseITaskFromPanel(panel);
                        DataHandler.genInstance().editITask(tNew);
                        updateTaskRows();
                        owner.updateScene();
                    } 
                }
            });
            row.add(editButton);

            JButton deleteButton = new JButton("Delete");
            deleteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataHandler.genInstance().removeITask(project);
                    updateTaskRows();
                    owner.updateScene();
                }
            });
            row.add(deleteButton);
            projectPanel.add(row);
        }
    }

    
    public void setParentDialog(ListTaskDialog dialog){
        this.owner = dialog;
    }
}

