/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics.panels;

import cz.fit.dpo.dpo_project_mav.controls.interfaces.Visitable;
import cz.fit.dpo.dpo_project_mav.controls.ViewHandler;
import cz.fit.dpo.dpo_project_mav.controls.interfaces.ITaskVisitable;
import cz.fit.dpo.dpo_project_mav.graphics.TaskPanelDialog;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Radek
 */
public abstract class ITaskPanel extends JPanel implements Visitable, ITaskVisitable{
    protected ViewHandler viewHandler;
    protected JTextField name;
    protected JTextField description;
    protected JButton cancel, ok;
    protected boolean validITask = false;
    protected TaskPanelDialog parentDialog;
    protected boolean readOnly;

    public ITaskPanel(ViewHandler viewHandler, boolean readOnly) {
        this.viewHandler = viewHandler;
        this.readOnly = readOnly;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        JPanel row = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel label = new JLabel("Name:");
        name = new JTextField(33);
        name.setEnabled(!readOnly);
        row.add(label);
        row.add(name);
        add(row);
        
        row = new JPanel(new FlowLayout(FlowLayout.LEFT));
        label = new JLabel("Description:");
        description = new JTextField(30);
        description.setEnabled(!readOnly);
        row.add(label);
        row.add(description);
        add(row);
    } 
    
    public void addButtons(){
        JPanel row = new JPanel(new FlowLayout(FlowLayout.CENTER));
        cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                validITask = false;
                parentDialog.dispose();
            }
        });
        row.add(cancel);
        
        ok = new JButton("OK");
        ok.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(!readOnly && !checkData()){
                    viewHandler.showWrongInput();
                    return;
                }
                validITask = !readOnly && true;
                parentDialog.dispose();
            }
        });
        row.add(ok);
        add(row);
    }
    
    public String getAttrName(){
        return name.getText();
    }
    
    public String getAttrDescription(){
        return description.getText();
    }
    
    public boolean isForSerialize() {
        return validITask;
    }
    
    public void setParentDialog(TaskPanelDialog parentDialog) {
        this.parentDialog = parentDialog;
    }
    
    public void copyOnlyData(ITaskPanel panelOrig){
        this.name = panelOrig.name;
        this.description = panelOrig.description;
        this.validITask = panelOrig.validITask;
    }
    
    public boolean isFilledEverything(){
        if(name.getText().equals("") || description.getText().equals(""))
            return false;
        return true;
    }
    
    protected abstract boolean checkData();
    
    public void setITaskData(ITask iTask){
        this.name.setText(iTask.getName());
        this.description.setText(iTask.getDescription());
    }
    
    //for testing only
    public JTextField getNameTextField() {
        return name;
    }

    public JTextField getDescriptionTextField() {
        return description;
    }

    public boolean isReadOnly() {
        return readOnly;
    }
    
}
