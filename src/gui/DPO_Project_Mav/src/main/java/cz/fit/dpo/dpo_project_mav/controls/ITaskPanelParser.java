/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.controls;

import cz.fit.dpo.dpo_project_mav.controls.visitors.PanelDataParserVisitor;
import cz.fit.dpo.dpo_project_mav.graphics.panels.ITaskPanel;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;

/**
 *
 * @author Radek
 */
public class ITaskPanelParser {

    public ITaskPanelParser() {
    }
    
    public ITask parseITaskFromPanel(ITaskPanel panel){
        return panel.parse(new PanelDataParserVisitor());
    }
}
