/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.models;

import cz.fit.dpo.modules.taskManager.printers.IOutputResult;
import cz.fit.dpo.modules.taskManager.printers.IOutputResultFactory;
import java.util.Iterator;

/**
 *
 * @author karel-pc
 */
public abstract class TaskComponent {

    protected IOutputResultFactory outputResultFactory = null;

    public abstract Integer calculateCost();

    public abstract Iterator<IOutputResult> getTaskIterator();

    public abstract void add();

    public abstract void edit();

    public abstract void remove();

    public void setOutputResultFactory(IOutputResultFactory outputResultFactory) {
        this.outputResultFactory = outputResultFactory;
    }
    
    
}
