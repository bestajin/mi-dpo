/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.visitors;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.modules.taskManager.models.TaskComponent;

/**
 *
 * @author karel-pc
 */
public interface ITaskFactoryVisitor {
    public TaskComponent createTaskComponent(ITask task);
}
