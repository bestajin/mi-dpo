/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.printers;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;



/**
 *
 * @author karel-pc
 */
public interface IOutputResult {
    public void createFromProject(Project project);
    public void createFromTask(Task task);
    public String getResult();
}
