/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager;

import cz.fit.dpo.filters.IProjectFilter;
import cz.fit.dpo.filters.ITaskFilter;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.io.File;
import java.util.List;


/**
 *
 * @author karel-pc
 */
public interface ITaskManager {
    public void add(ITask task);
    public void edit(ITask task);
    public void remove(ITask task);
    public List<Project> getProjects(IProjectFilter projectFilter);
    public List<Task> getTasks(ITaskFilter taskFilter);
    public File exportToCSV(ITask task);
    public Task getTaskById(Long id);
    public Project getProjectById(Long id);
    public Integer getCost(ITask task);
}
