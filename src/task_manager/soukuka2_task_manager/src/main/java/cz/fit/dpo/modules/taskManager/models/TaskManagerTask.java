/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.models;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.util.Iterator;

import cz.fit.dpo.modules.dbConnector.DBConnector;
import cz.fit.dpo.modules.taskManager.iterators.TaskIterator;
import cz.fit.dpo.modules.taskManager.printers.IOutputResult;

/**
 *
 * @author karel-pc
 */
public class TaskManagerTask extends TaskComponent {

    protected Task task;
    protected DBConnector dbConnector;


    public TaskManagerTask(Task task, DBConnector dbConnector) {
        this.task = task;
        this.dbConnector = dbConnector;
        
    }

    @Override
    public Integer calculateCost() {
        return (Integer) task.getCosts();
    }

    @Override
    public Iterator<IOutputResult> getTaskIterator() {
        if (outputResultFactory == null) {
            throw new RuntimeException("output result factory isnt set.");
        }
        
        return new TaskIterator(task, outputResultFactory);
    }

    @Override
    public void add() {
        dbConnector.addTask(task);
    }

    @Override
    public void edit() {
        dbConnector.editTask(task);
    }

    @Override
    public void remove() {
        dbConnector.removeTask(task);
    }

    
    
}
