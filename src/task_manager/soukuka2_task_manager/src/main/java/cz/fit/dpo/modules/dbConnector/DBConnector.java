/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.dbConnector;

import cz.fit.dpo.filters.IProjectFilter;
import cz.fit.dpo.filters.ITaskFilter;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.util.List;

/**
 *
 * @author karel-pc
 */
public interface DBConnector {
    public void addProject(Project project);
    public void addTask(Task task);
    public void editProject(Project project);
    public void editTask(Task task);
    public List<Project> getProjects(IProjectFilter projectFilter);
    public List<Task> getTasks(ITaskFilter taskFilter);
    public void removeProject(Project project);
    public void removeTask(Task task);
    public void closeConnection();
}
