/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.printers;

/**
 *
 * @author karel-pc
 */
public class CSVResultFactory implements IOutputResultFactory{
    @Override
    public IOutputResult createOutputResult() {
        return new CSVRowResult();
    }
}
