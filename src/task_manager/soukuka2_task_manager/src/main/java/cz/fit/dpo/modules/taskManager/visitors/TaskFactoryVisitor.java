/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.visitors;


import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITaskVisitor;
import cz.fit.dpo.modules.dbConnector.DBConnector;
import cz.fit.dpo.modules.taskManager.models.TaskComponent;
import cz.fit.dpo.modules.taskManager.models.TaskManagerProject;
import cz.fit.dpo.modules.taskManager.models.TaskManagerTask;
import cz.fit.dpo.modules.taskManager.printers.IOutputResultFactory;

/**
 *
 * @author karel-pc
 */
public class TaskFactoryVisitor implements ITaskVisitor, ITaskFactoryVisitor {

    protected TaskComponent taskComponent;
    protected DBConnector dbConnector;
    protected IOutputResultFactory outputResultFactory;

    public TaskFactoryVisitor(DBConnector dbConnector) {
        this.dbConnector = dbConnector;
    }
    
    @Override
    public void visit(Project project) {
        taskComponent = new TaskManagerProject(project, dbConnector);
    }

    @Override
    public void visit(Task task) {
        taskComponent = new TaskManagerTask(task, dbConnector);
    }
  
    @Override
    public TaskComponent createTaskComponent(ITask task) {
        taskComponent = null;
        
        task.acceptVisitor(this);
        
        return taskComponent;
    }
}
