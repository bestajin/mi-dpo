/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITaskVisitor;
import cz.fit.dpo.modules.dbConnector.DBConnector;

/**
 *
 * @author karel-pc
 */
public interface ITaskManagerFactory {
    public ITaskManager createTaskManager(DBConnector dbConnector);
}
