/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.printers;

/**
 *
 * @author karel-pc
 */
public interface IOutputResultFactory {
    public IOutputResult createOutputResult();
}
