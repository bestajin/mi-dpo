/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.printers;


import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;

/**
 *
 * @author karel-pc
 */
public class CSVRowResult implements IOutputResult {
    protected boolean isProject = false;
    protected String name;
    protected String desrcription;
    protected String owner;
    protected String state;
    protected String numSubProjects = "";
    protected String start = "";
    protected String finish = "";
    protected String costs = "";
    
    @Override
    public void createFromProject(Project project) {
        initializeITaskParams(project);
        isProject = true;
        numSubProjects = ""+project.getProjects().size();
    }

    @Override
    public void createFromTask(Task task) {
        initializeITaskParams(task);
        start = task.getStart().toString();
        finish = task.getFinish().toString();
        costs = task.getCosts().toString();
    }
    
    private void initializeITaskParams(ITask task) {
        name = task.getName();
        desrcription = task.getDescription();
        owner = task.getOwner().getFirstName() + " " + task.getOwner().getLastName();
        state = task.getState().getDescription();
    }
    
    @Override
    public String getResult() {
        return "\"" + name + "\"," + 
               "\"" + desrcription + "\"," +
                "\"" + owner + "\"," + 
                "\"" + state + "\"," + 
                "\"" + (isProject ? numSubProjects : "") + "\"," + 
                "\"" + (! isProject ? start : "") + "\"," + 
                "\"" + (! isProject ? finish : "") + "\"," + 
                "\"" + (! isProject ? costs : "") + "\"\n";
    }

    
    
}
