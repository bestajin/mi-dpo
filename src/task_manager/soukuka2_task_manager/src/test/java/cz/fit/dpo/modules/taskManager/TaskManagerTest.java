/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager;

import cz.fit.dpo.filters.ProjectFilter;
import cz.fit.dpo.filters.TaskFilter;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.State;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import cz.cvut.fit.spravaprojektu.usermanager.UserModel;


import cz.fit.dpo.modules.dbConnector.DBConnector;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author karel-pc
 */
public class TaskManagerTest {
    
    HashMap<Long,ITask> storage;
    ITaskManager taskManager;
    
    public TaskManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        storage = new HashMap<>();        
        
        DBConnector dbConnector = new SimpleDbConnector(storage);
        
        TaskManagerFactory taskManagerFactory = new TaskManagerFactory();
        taskManager = taskManagerFactory.createTaskManager(dbConnector);
    }
    
    @After
    public void tearDown() {
    }
    
    protected Task createTask() {
        Task task;
        task = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        task.setCosts(10);
        task.setName("Create DB connector");
        task.setOwner(new UserModel(""));
        task.setState(new State(""));
        task.setStart(new Date());
        task.setFinish(new Date());
        
        return task;
    }

    protected Project createProject() {
        Project project = new Project(new LinkedList<ITask>(), "", "", null, null);
        project.setName("Create DB connector");
        project.setOwner(new UserModel(""));
        project.setState(new State(""));
        Collection<ITask> projects = new ArrayList<>();
        project.setProjects(projects);
        
        project.getProjects().add(createTask());
        project.getProjects().add(createTask());
        
        return project;
    }
    
    protected Project createProjectWithSubProjects() {
        Project project = createProject();
        
        project.getProjects().add(createProject());
        project.getProjects().add(createProject());
                
        return project;
    }
    
    /**
     * Test of add method, of class TaskManager.
     */
    @Test
    public void testAdd() {        
        Task task = createTask();
        Project project = createProject();
        
        taskManager.add(task);
        taskManager.add(project);
        
        assertTrue(storage.containsValue(task));
        assertTrue(storage.containsValue(project));
    }
    
    @Test
    public void testEdit() {        
        Task task = createTask();
        Project project = createProject();
        
        taskManager.add(task);
        taskManager.add(project);
    
        Task task2 = createTask();
        task2.setId(task.getId());
        task2.setName("new name");
        
        
        Project project2 = createProject();
        project2.setId(project.getId());
        
        taskManager.edit(task2);
        taskManager.edit(project2);
        
        assertTrue(storage.containsValue(task2));
        assertTrue(storage.containsValue(project2));
    }
    
    @Test
    public void testRemove() {        
        Task task = createTask();
        Project project = createProject();
        
        taskManager.add(task);
        taskManager.add(project);

        
        taskManager.remove(task);
        taskManager.remove(project);
             
        assertFalse(storage.containsValue(task));
        assertFalse(storage.containsValue(project));
    }

    @Test
    public void testExportToCSV() {        
        Task task = createTask();
        
        File file = taskManager.exportToCSV(task);        
        assertNotNull(file);
        
        
        File file2 = taskManager.exportToCSV(createProject());        
        assertNotNull(file2);
    }

    @Test
    public void testCalculateCost() {        
        Task task = createTask();
        Project project = createProject();
        Project projectWithSubProjects = createProjectWithSubProjects();
       
        assertSame(taskManager.getCost(task), 10);
        assertSame(taskManager.getCost(project), 20);
        assertSame(taskManager.getCost(projectWithSubProjects),60);
    }
    
    @Test
    public void testGetProjects() {        
        Project project = createProject();
        project.setName("project numero 5");
        taskManager.add(project);
   
        ProjectFilter projectFilter = new ProjectFilter();
        projectFilter.setName("project numero 5");
        List<Project> projects = taskManager.getProjects(projectFilter);
        
        assertTrue(projects.contains(project));
        
        
        ProjectFilter projectFilter2 = new ProjectFilter();
        projectFilter.setName("blaba project 14");
        List<Project> projects2 = taskManager.getProjects(projectFilter2);
        
        assertFalse(projects2.contains(project));
    }
    
    @Test
    public void testGetTask() {        
        Task task = createTask();
        task.setName("task numero 5");
        taskManager.add(task);
   
        TaskFilter taskFilter = new TaskFilter();
        taskFilter.setName("task numero 5");
        List<Task> tasks = taskManager.getTasks(taskFilter);
        
        assertTrue(tasks.contains(task));
        
        
        TaskFilter taskFilter2 = new TaskFilter();
        taskFilter.setName("task numero spatne 50");
        List<Task> tasks2 = taskManager.getTasks(taskFilter2);
        
        assertFalse(tasks2.contains(task));
    }
    
    @Test
    public void testGetTaskById() {        
        Task task = createTask();
        
        taskManager.add(task);
        Long taskId = task.getId();
   
        assertSame(task, taskManager.getTaskById(taskId)); 
        
        assertNull(taskManager.getTaskById(new Long(12345)));
    }
    
    @Test
    public void testGetProjectById() {        
        Project project = createProject();
        
        taskManager.add(project);
        Long projectId = project.getId();
   
        assertSame(project, taskManager.getProjectById(projectId)); 
        
        assertNull(taskManager.getProjectById(new Long(12345)));
    }
}