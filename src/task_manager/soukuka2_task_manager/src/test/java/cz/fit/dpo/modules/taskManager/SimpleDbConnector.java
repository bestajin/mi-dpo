/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager;

import cz.fit.dpo.filters.IProjectFilter;
import cz.fit.dpo.filters.ITaskFilter;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;

import cz.fit.dpo.modules.dbConnector.DBConnector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 *
 * @author karel-pc
 */
public class SimpleDbConnector implements DBConnector {

    HashMap<Long, ITask> storage;
    Long nextId = new Long(1);

    public SimpleDbConnector(HashMap<Long, ITask> storage) {
        this.storage = storage;
    }

    @Override
    public void addProject(Project project) {
        project.setId(nextId);
        nextId++;
        storage.put(project.getId(), project);
    }

    @Override
    public void addTask(Task task) {
        task.setId(nextId);
        nextId++;
        storage.put(task.getId(), task);
    }

    @Override
    public void editProject(Project project) {
        storage.put(project.getId(), project);
    }

    @Override
    public void editTask(Task task) {
        storage.put(task.getId(), task);
    }

    @Override
    public List<Project> getProjects(IProjectFilter projectFilter) {
        List<Project> projects = new ArrayList<>();
        if (projectFilter.getName() != null) {
            for (Entry<Long, ITask> entry : storage.entrySet()) {             
                if (entry.getValue().getName().equals(projectFilter.getName())) {
                    projects.add((Project) entry.getValue());
                }
            }
        }
        if (projectFilter.getId() != null) {
            for (Entry<Long, ITask> entry : storage.entrySet()) {             
                if (entry.getValue().getId().equals(projectFilter.getId())) {
                    projects.add((Project) entry.getValue());
                }
            }
        }
        return projects;
    }

    @Override
    public List<Task> getTasks(ITaskFilter taskFilter) {
        List<Task> tasks = new ArrayList<>();
        if (taskFilter.getName() != null) {
            for (Entry<Long, ITask> entry : storage.entrySet()) {             
                if (entry.getValue().getName().equals(taskFilter.getName())) {
                    tasks.add((Task) entry.getValue());
                }
            }
        }
        if (taskFilter.getId() != null) {
            for (Entry<Long, ITask> entry : storage.entrySet()) {             
                if (entry.getValue().getId().equals(taskFilter.getId())) {
                    tasks.add((Task) entry.getValue());
                }
            }
        }
        return tasks;
    }

    @Override
    public void removeProject(Project project) {
        storage.remove(project.getId());
    }

    @Override
    public void removeTask(Task task) {
        storage.remove(task.getId());
    }

    @Override
    public void closeConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
